module DiffGeom.Spaces.QuaternionsTest where

import Test.Tasty
import Test.Tasty.QuickCheck as QC
import Test.QuickCheck

import DiffGeom.Spaces.Quaternions
import DiffGeom.Spaces.Vec3
import DiffGeom.Spaces.Vec3Test

import DiffGeom.Classes.LieTest
import DiffGeom.Classes.VectorSpaceTest
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Riem
import DiffGeom.Classes.RiemTest

import Test.Generic
import Data.Tagged

instance Arbitrary Quatern where
    arbitrary = do
        s <- arbitrarySizedFractional
        v <- arbitrary
        return $ Quatern s v

quaternTests :: TestTree
quaternTests = testGroup "Non-Generic Tests" []

allQuaternTests = testGroup "Quatern" $ quaternTests : [cgp]
  where
    cgp = testGroup "Classes" $ map unTagged (t :: [Tagged Quatern TestTree])
    t = allMonoidTests ++ allRealInnerProductSpaceTests
