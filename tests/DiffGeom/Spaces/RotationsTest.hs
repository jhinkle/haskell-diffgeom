module DiffGeom.Spaces.RotationsTest where

import Test.Tasty
import Test.Tasty.QuickCheck as QC
import Test.QuickCheck

import DiffGeom.Spaces.Rotations
import DiffGeom.Spaces.Quaternions
import DiffGeom.Spaces.QuaternionsTest
import DiffGeom.Spaces.Vec3
import DiffGeom.Spaces.Vec3Test

import DiffGeom.Classes.LieTest
import DiffGeom.Classes.VectorSpace
import DiffGeom.Classes.VectorSpaceTest
import DiffGeom.Classes.Smooth
import DiffGeom.Classes.Riem
import DiffGeom.Classes.RiemTest

import Test.Generic
import Data.Tagged

instance Arbitrary Rot3 where
    arbitrary = do
        p <- arbitrary `suchThat` (\x -> normSquared x > 1)
        return $ Rot3 $ normalizeUnsafe p

rot3Tests :: TestTree
rot3Tests = testGroup "Non-Generic Tests" []

allRot3Tests :: TestTree
allRot3Tests = testGroup "Rot3" $ rot3Tests : [cgp]
  where
    cgp = testGroup "Classes" $ map unTagged (t :: [Tagged Rot3 TestTree])
    t = allGroupTests
